= How to hack on Shimmer

Here is how you can get started working on Shimmer:

1. Tell me your GitLab account so I can add you to the project members.

2. Have the Go compiler and build tool installed.
   You will also want to install shellcheck for running the
   validation/regression tests.

3. Clone the repo from git@gitlab.com:esr/shimmer.git because
   we're not self-hosting yet; that should be coming soon.

4. Read design-notes.adoc

5. Skim or read shimmer.adoc

6. Skim the code in shimmer.go. If you are working on the Web front
   end, you will probably mostly be modifying the four page templates
   past line 1900. You'll want to read up on Go's HTML
   templating system, those docs are at https://pkg.go.dev/html/template

That will get you started.  There are three ways to test changes:

1. Run "make check". Please don't do a push or MR unless it passes.

2. The shimmertest script creates a couple of small test repositories, then runs the
   command you give it. On exit, the toy test repos are removed.  Read
   the header comment of that script to learn about simple test commands.

3. In particular, "shimmertest serve" brings up a webserver you can use to explore
   Shimmer's operation in small test repositories.

// end

